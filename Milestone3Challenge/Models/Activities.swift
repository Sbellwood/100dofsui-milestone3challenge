//
//  Activities.swift
//  Milestone3Challenge
//
//  Created by Skyler Bellwood on 7/25/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import Foundation

class Activities: ObservableObject {
    private let activityUserDefaultKey = "Activities"
    
    @Published var activityCollection: [Activity] {
        didSet {
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(activityCollection) {
                UserDefaults.standard.set(encoded, forKey: activityUserDefaultKey)
            }
        }
    }
    
    init() {
        
        if let activities = UserDefaults.standard.data(forKey: activityUserDefaultKey) {
            let decoder = JSONDecoder()
            if let decoded = try? decoder.decode([Activity].self, from: activities) {
                self.activityCollection = decoded
                return
            }
        }
        
        self.activityCollection = []
    }
}
