//
//  ActivityItem.swift
//  Milestone3Challenge
//
//  Created by Skyler Bellwood on 7/25/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import Foundation

struct Activity: Identifiable, Codable {
    let id = UUID()
    let title: String
    let description: String
    let completedCount: Int
}
