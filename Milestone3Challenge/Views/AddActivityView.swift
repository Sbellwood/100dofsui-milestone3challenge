//
//  AddActivityView.swift
//  Milestone3Challenge
//
//  Created by Skyler Bellwood on 7/25/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import SwiftUI

struct AddActivityView: View {
    
    @ObservedObject var activities: Activities
    @Environment(\.presentationMode) var presentationMode
    
    @State private var title: String = ""
    @State private var description: String = ""
    @State private var completedCount: Int = 0
    
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("Activity").font(.headline)) {
                    TextField("Title", text: $title)
                }
                
                Section(header: Text("Activity Info").font(.headline)) {
                    
                    // Switch to TextEditor in iOS 14
                    TextField("Description", text: $description)
                    Stepper(value: $completedCount, in: 0...1000) {
                        Text("Number of times completed: \(completedCount)")
                    }
                }
                
            }
        .navigationBarTitle("Add new activity")
            .navigationBarItems(trailing: Button("Save"){
                let activity = Activity(title: self.title, description: self.description, completedCount: self.completedCount)
                self.activities.activityCollection.append(activity)
                self.presentationMode.wrappedValue.dismiss()
            })
        }
    }
}

struct AddActivityView_Previews: PreviewProvider {
    static var previews: some View {
        AddActivityView(activities: Activities())
    }
}
