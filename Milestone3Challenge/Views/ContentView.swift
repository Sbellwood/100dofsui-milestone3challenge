//
//  ContentView.swift
//  Milestone3Challenge
//
//  Created by Skyler Bellwood on 7/25/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject var activities = Activities()
    @State private var showingAddActivity = false
    
    var body: some View {
        NavigationView {
            List {
                ForEach(activities.activityCollection) { activity in
                    NavigationLink(destination: DetailActivityView(activities: self.activities, activityID: activity.id)) {
                        HStack {
                            VStack(alignment: .leading) {
                                Text(activity.title)
                                    .font(.headline)
                                Text(activity.description)
                                    .font(.subheadline)
                            }
                            
                            Spacer()
                            
                            Text(activity.completedCount == 1 ? "Completed \(activity.completedCount) time" : "Completed: \(activity.completedCount) times")
                        }
                    }
                }
                .onDelete(perform: removeActivity)
            }
            .navigationBarTitle("iActivity")
            .navigationBarItems(trailing:
                HStack {
                    EditButton()
                        .padding(.horizontal)
                    Button(action: {
                        self.showingAddActivity = true
                    }) {
                        Image(systemName: "plus")
                    }
                }
            )
        }
        .sheet(isPresented: $showingAddActivity) {
            AddActivityView(activities: self.activities)
        }
    }
    
    func removeActivity(at offsets: IndexSet) {
        activities.activityCollection.remove(atOffsets: offsets)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
