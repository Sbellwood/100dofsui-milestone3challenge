//
//  DetailActivityView.swift
//  Milestone3Challenge
//
//  Created by Skyler Bellwood on 7/25/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import SwiftUI

struct DetailActivityView: View {
    
    @ObservedObject var activities: Activities
    
    var activityID: UUID
    var activity: Activity
    
    init(activities: Activities, activityID: UUID) {
        self.activities = activities
        self.activityID = activityID
        
        if let activity = activities.activityCollection.first(where: { $0.id == activityID }) {
            self.activity = activity
        } else {
            self.activity = Activity(title: "Test", description: "Test Description", completedCount: 4)
        }
    }
    
    var body: some View {
        VStack() {
            Form {
                Section(header: Text("Description").font(.headline)) {
                    Text(self.activity.description)
                }
                
                Section(header: Text("Completion Count").font(.headline)) {
                    Text("\(self.activity.completedCount)")
                    Button("Complete \(activity.title)") {
                        if let updatingActivity = self.activities.activityCollection.first(where: { $0.id == self.activityID }) {
                            
                            // TODO: figure out how to bind this button to @ObservedObject activities
                            print("Complete \(updatingActivity.title) tapped")
                        }
                        
                    }
                }
            }
        }
        .navigationBarTitle(Text(activity.title), displayMode: .inline)
    }
}

struct DetailActivityView_Previews: PreviewProvider {
    static var previews: some View {
        DetailActivityView(activities: Activities(), activityID: UUID())
    }
}
